# 1st Screen - aOS Data Analytics SDK

###

Documentation and files for 1st Screen clients that are integrating the aOS Data Analytics SDK

This SDK provides an API set that allows apps owners to collects the full breadth of user app data for the purpose of commercialisation through advertising and remarketing


---


## SDK Build

### Current Version 1.0.0


Add the following to the top level `build.gradle`
```groovy
allprojects {
    repositories {
        jcenter()
        maven {
            url "https://maven.google.com"
        }

        maven {
            url 'https://bitbucket.org/webfitnz/1stscreen-maven/raw/8483cb6/releases/aar/?at=master'
        }
    }
}
```
 
then add the SDK dependency to the app level `build.gradle`
```groovy
compile('com.firstscreen:analytics:1.0.0@aar') {
        transitive = true
    }
```

### Dependencies:

The SDK has a minimal list of dependancies that we update during our quarterly release cycle. We always aim to reduce the footprint of our technology

* com.android.evernote.job
* Keen Client API
* Keen Client Java
* net.vrallev.android


---


## Usage


### Initialization:


Initialize the `FSAnalyticsTracker` inside the `onCreate` method of your `Application` subclass.
```java
@Override
public void onCreate() {
    super.onCreate();

    FSAnalyticsTracker.init(this, false, "TREASURE-DATA-WRITE-KEY", "YOUR-ENCRYPTION-KEYWORD");
}
```
`FSAnalyticsTracker` requires a `Configuration` for setup. The above method looks for a configuration JSON file in the app's
`assets` folder. The default configuration file names for Sandbox and Production mode are respectively `fs_config_sandbox.json`
and `fs_config.json`. Here's the things you can define in the configuration file:

###


_**Mandatory** paramters to include in the configuration file:_
 
_Key_                        |_Type_     |_Explanation_
----------------------------|---------|-----------------------------------------------------------------
isSandboxMode               | boolean | Is in sandbox/debug mode
publisherId                 | String  | Publisher/Partner ID
buid                        | String  | App Bundle ID.

_**Optional** paramters to include in the configuration file:_

_Key_                        |_Type_    |_Explanation_
----------------------------|---------|-----------------------------------------------------------------
dataCollectionOff           | boolean | If true then only anonymous SDK log data is collected
reasonForNotTracking        | String  | Select from - coppa / consent / gdpr
apparea                     | String  | App Activity Name
siid                        | String  | App Activity ID
kews                        | String  | Specific Activity Information - Array of JSON style key value pairs
uage                        | String  | Users Age
usge                        | String  | Users Gender
pode                        | String  | Users Post Code.

###

These optional parameters have been standardised, by 1st Screen, based on our client activity to date. This is not an exhaustive list and any custom parameter can be added. Please ensure to communicate any additional parameters during the integration process to ensure full commercialisation of the data

If you want your config file to be named differently than the default then you can provide the name of that file while initializng the tracker.

```java
FSAnalyticsTracker.init(this, false, "API-KEY", "ENCRYPTION-KEY", "CONFIG-FILE_NAME");
```


### Manual Initialization:


You can also build a `Configuration` manually in code and then use that to initialize the tracker.
```java
Configuration configuration = Configuration.builder()
                    .withIsSandboxMode(false)
                    .withApiKey(API_KEY)
                    .withPublisherId("PARTNER-ID")
                    .withDataCollectionOff(false)
                    .build();
    
FSAnalyticsTracker.init(this, configuration);
```


### Custom Event:


You can track a simple event like this:
```java
final String MY_EVENT_TYPE = "MY_EVENT";
FSEvent event = new FSEvent(MY_EVENT_TYPE, "this is a simple event");
Map<String, Object> eventMap = FSAnalyticsTracker.Util.getMapFromObject(event, FSEvent.class);
FSAnalyticsTracker.getSharedInstance().addEventRecord(null, null, eventMap, 
    new KeenCallback() {
        @Override
        public void onSuccess() {

        }

        @Override
        public void onFailure(Exception e) {

        }
});
```
 
Activity visits/Page view events are automatically tracked. You can provide custom page names for activities by making 
your `Activity` implement `PageNameProvider`.
 
We have provided the following helper methods to assist with user data that will remain static throughout the session:
```java
FSAnalyticsTracker.getSharedInstance().setUserAge(32);
FSAnalyticsTracker.getSharedInstance().setUserGender("M");
FSAnalyticsTracker.getSharedInstance().setUserPostCode("2212");
```

### Custom Data:


You can add additional data to your event using:
```java
Map<String, Object> dataMap = FSAnalyticsTracker.getSharedInstance().addAdditionalData(eventMap);
```


---


## Deleting Data


You can request for deletion of all data for this specific device/user using:
```java
FSAnalyticsTracker.getSharedInstance().requestToDeleteEvents("REASON-FOR-DELETION");
```

This API has been included with the intention of it being built into a button that can be included on the privacy page of the application. This would allow users to request the deletion of their data. All user data collected by 1st Screen's SDK and recored against the users Advertsiing ID will be deleted after this API is triggered. Future data will still be collected and its up the user to restrict tracking or the application to restrict data collection using the configuration file.

When using this API we reccomend that a message similar to the below is presented to the user

"Your request has been sent and your historical data will be deleted within a reasonable timeframe".